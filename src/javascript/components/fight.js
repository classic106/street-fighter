import { controls } from '../../constants/controls';

const fightersHealth = {
  one: 0,
  two: 0,
  firstFighter: null,
  secondFighter: null,
  leftIndicator: null,
  rightIndicator: null,
  leftWidth: 0,
  rightWidth: 0,
  leftWidthInit: 0,
  rightWidthInit: 0,

  get winner() {
    return new Promise((resolve) => {
      if (this.one <= 0) {
        return resolve(this.secondFighter);
      } else if (this.two <= 0) {
        return resolve(this.firstFighter);
      }
    });
  }
};

const criticalHits = { one: true, two: true };

const {
  PlayerOneAttack,
  PlayerOneBlock,
  PlayerTwoAttack,
  PlayerTwoBlock,
  PlayerOneCriticalHitCombination,
  PlayerTwoCriticalHitCombination
} = controls;

const firstFighterButtons = {
  [PlayerOneAttack]: false,
  [PlayerOneBlock]: false,
  [PlayerOneCriticalHitCombination[0]]: false,
  [PlayerOneCriticalHitCombination[1]]: false,
  [PlayerOneCriticalHitCombination[2]]: false
};

const secondFighterButtons = {
  [PlayerTwoAttack]: false,
  [PlayerTwoBlock]: false,
  [PlayerTwoCriticalHitCombination[0]]: false,
  [PlayerTwoCriticalHitCombination[1]]: false,
  [PlayerTwoCriticalHitCombination[2]]: false
};

function keyDown(firstFighter, secondFighter, event) {
  const { code } = event;

  switch (code) {
    case PlayerOneAttack:
      firstFighterButtons[code] = true;
      break;
    case PlayerOneBlock:
      firstFighterButtons[code] = true;
      break;
    case PlayerOneCriticalHitCombination[0]:
      firstFighterButtons[code] = true;
      break;
    case PlayerOneCriticalHitCombination[1]:
      firstFighterButtons[code] = true;
      break;
    case PlayerOneCriticalHitCombination[2]:
      firstFighterButtons[code] = true;
      break;

    case PlayerTwoAttack:
      secondFighterButtons[code] = true;
      break;
    case PlayerTwoBlock:
      secondFighterButtons[code] = true;
      break;
    case PlayerTwoCriticalHitCombination[0]:
      secondFighterButtons[code] = true;
      break;
    case PlayerTwoCriticalHitCombination[1]:
      secondFighterButtons[code] = true;
      break;
    case PlayerTwoCriticalHitCombination[2]:
      secondFighterButtons[code] = true;
      break;
    default:
      break;
  }
  calculateDamage(firstFighter, secondFighter);
}

function keyUp(firstFighter, secondFighter, event) {
  const { code } = event;

  switch (code) {
    case PlayerOneAttack:
      firstFighterButtons[code] = false;
      break;
    case PlayerOneBlock:
      firstFighterButtons[code] = false;
      break;
    case PlayerOneCriticalHitCombination[0]:
      firstFighterButtons[code] = false;
      break;
    case PlayerOneCriticalHitCombination[1]:
      firstFighterButtons[code] = false;
      break;
    case PlayerOneCriticalHitCombination[2]:
      firstFighterButtons[code] = false;
      break;

    case PlayerTwoAttack:
      secondFighterButtons[code] = false;
      break;
    case PlayerTwoBlock:
      secondFighterButtons[code] = false;
      break;
    case PlayerTwoCriticalHitCombination[0]:
      secondFighterButtons[code] = false;
      break;
    case PlayerTwoCriticalHitCombination[1]:
      secondFighterButtons[code] = false;
      break;
    case PlayerTwoCriticalHitCombination[2]:
      secondFighterButtons[code] = false;
      break;
    default:
      break;
  }
  calculateDamage(firstFighter, secondFighter);
}

function randomInteger() {
  return 1 + Math.random();
}

function calculateDamage(firstFighter, secondFighter) {
  const {
    [PlayerOneAttack]: attackOne,
    [PlayerOneBlock]: blockOne,
    [PlayerOneCriticalHitCombination[0]]: criticalOne,
    [PlayerOneCriticalHitCombination[1]]: criticalOne1,
    [PlayerOneCriticalHitCombination[2]]: criticalOne2
  } = firstFighterButtons;
  const {
    [PlayerTwoAttack]: attackTwo,
    [PlayerTwoBlock]: blockTwo,
    [PlayerTwoCriticalHitCombination[0]]: criticalTwo,
    [PlayerTwoCriticalHitCombination[1]]: criticalTwo1,
    [PlayerTwoCriticalHitCombination[2]]: criticalTwo2
  } = secondFighterButtons;

  function setCriticalHit(fighter) {
    criticalHits[fighter] = false;
    setTimeout(function () {
      criticalHits[fighter] = true;
    }, 10000);
  }

  function leftFighterIndicator(damage, defender) {
    const c = defender.health / damage;
    fightersHealth.leftWidth =
      Number.parseInt(fightersHealth.leftWidth) - Number.parseInt(fightersHealth.leftWidthInit) / c + 'px';
    fightersHealth.leftIndicator.style.width = fightersHealth.leftWidth;
  }

  function rightFighterIndicator(damage, defender) {
    const c = defender.health / damage;
    fightersHealth.rightWidth =
      Number.parseInt(fightersHealth.rightWidth) - Number.parseInt(fightersHealth.rightWidthInit) / c + 'px';
    fightersHealth.rightIndicator.style.width = fightersHealth.rightWidth;
  }

  if ((attackOne && blockTwo) || attackOne) {
    const damage = getDamage(firstFighter, secondFighter);
    if (damage) {
      fightersHealth.two -= damage;
      rightFighterIndicator(damage, firstFighter);
    }
  } else if ((attackTwo && blockOne) || attackTwo) {
    const damage = getDamage(secondFighter, firstFighter);
    if (damage) {
      fightersHealth.one -= damage;
      leftFighterIndicator(damage, firstFighter);
    }
  } else if (criticalOne && criticalOne1 && criticalOne2) {
    if (criticalHits.one) {
      fightersHealth.two -= firstFighter.attack * 2;
      setCriticalHit('one');
      rightFighterIndicator(secondFighter.attack * 2, firstFighter);
    }
  } else if (criticalTwo && criticalTwo1 && criticalTwo2) {
    if (criticalHits.two) {
      fightersHealth.one -= secondFighter.attack * 2;
      setCriticalHit('two');
      leftFighterIndicator(firstFighter.attack * 2, secondFighter);
    }
  } /*else if (attackOne) {
    fightersHealth.two -= firstFighter.attack;
    rightFighterIndicator(secondFighter.attack, secondFighter);
  } else if (attackTwo) {
    fightersHealth.one -= secondFighter.attack;
    leftFighterIndicator(firstFighter.attack, firstFighter);
  }*/ else if ((attackOne && blockOne) || (attackTwo && blockTwo)) {
    return;
  }
}

export async function fight(firstFighter, secondFighter) {
  fightersHealth.one = firstFighter.health;
  fightersHealth.two = secondFighter.health;

  fightersHealth.firstFighter = firstFighter;
  fightersHealth.secondFighter = secondFighter;

  fightersHealth.leftIndicator = document.querySelector('#left-fighter-indicator');
  fightersHealth.leftWidth = window.getComputedStyle(fightersHealth.leftIndicator).width;
  fightersHealth.leftWidthInit = fightersHealth.leftWidth;

  fightersHealth.rightIndicator = document.querySelector('#right-fighter-indicator');
  fightersHealth.rightWidth = window.getComputedStyle(fightersHealth.rightIndicator).width;
  fightersHealth.rightWidthInit = fightersHealth.rightWidth;

  document.addEventListener('keyup', keyUp.bind(null, firstFighter, secondFighter));
  document.addEventListener('keydown', keyDown.bind(null, firstFighter, secondFighter));

  let timer = null;

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    timer = setInterval(async () => resolve(await fightersHealth.winner), 500);
  }).then((result) => {
    clearInterval(timer);

    document.removeEventListener('keyup', keyUp);
    document.removeEventListener('keydown', keyDown);

    return result;
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  if (blockPower > hitPower) {
    return 0;
  }
  return hitPower - blockPower;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = randomInteger();
  const power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = randomInteger();
  const power = fighter.defense * dodgeChance;
  return power;
}
