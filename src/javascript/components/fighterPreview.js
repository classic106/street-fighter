import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  let positionClassName = '';

  if (position === 'right') {
    positionClassName = 'fighter-preview___right';
  } else if (position === 'left') {
    positionClassName = 'fighter-preview___left';
  }

  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  if (!fighter) return fighterElement;
  // todo: show fighter info (image, name, health, etc.)
  const fighterImage = createFighterImage(fighter);
  const fighterInfo = createElement({
    tagName: 'div',
    className: 'fighters___fighter info'
  });

  fighterInfo.innerHTML = `
    <p>Name: ${fighter.name}</p>
    <p>Health: ${fighter.health}</p>
    <p>Attack: ${fighter.attack}</p>
    <p>Defense: ${fighter.defense}</p>
  `;

  fighterElement.append(fighterImage);
  fighterElement.append(fighterInfo);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}
