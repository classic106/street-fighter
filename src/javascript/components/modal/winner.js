import { showModal } from './modal';
import { createFighterPreview } from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function
  showModal({
    title: fighter.name,
    bodyElement: createFighterPreview(fighter, ''),
    onClose: () => {
      console.log('close');
    }
  });
}
