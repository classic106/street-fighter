import { callApi } from '../helpers/apiHelper';

class FighterService {
  #endpoint = 'fighters.json';

  async getFighters() {
    try {
      const apiResult = await callApi(this.#endpoint);
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    this.#endpoint = `details/fighter/${id}.json`;
    const result = await this.getFighters();
    this.#endpoint = 'fighters.json';
    return result;
  }
}

export const fighterService = new FighterService();
